import { Inject } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { inject } from '@angular/core/testing';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-file-uploader',
  templateUrl: './file-uploader.component.html',
  styleUrls: ['./file-uploader.component.scss']
})
export class FileUploaderComponent implements OnInit {

  constructor(
    public api : ApiService,
   public dialogRef:MatDialogRef<FileUploaderComponent>,
   @Inject(MAT_DIALOG_DATA) public dialogData:any
  ) { }
   

  ngOnInit(): void {
    console.log(this.dialogData);
  }

selectedFile:any;   
onFileChange(event: any) {
if(event.target.files.length > 0) {
       this.selectedFile=event.target.files[0];
       console.log(this.selectedFile);        
   }
   }
   
loadingUpload: boolean|undefined;
uploadFile() {
  let input = new FormData();
  input.append('file', this.selectedFile);
  this.loadingUpload = true;  
  this.api.upload(input).subscribe((data:any)=>{    
    this.updateProduct(data);
    console.log(data);
    
  },(error: any)=>{
      this.loadingUpload = false;
      alert('Gagal mengunggah file');
  });
}


updateProduct(data:any)
 {
   if(data.status == true)
   {
     //lakukan update data produk disini
     this.UpdateBook(data);
     //lengkapi dlu fungsi upload, di cek lg fungsi di dalamnya
     alert('File berhasil diunggah');
     this.loadingUpload = false;
     //kode tambahan
     this.dialogRef.close();
   }else{
     alert(data.message);
   }
 }

UpdateBook(data:any)
{
  this.api.put('book/'+this.dialogData.id,{url: data.url}).subscribe(res=>{
    console.log(res);
  })
}



}
