import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  items : any;
  constructor(
    public api: ApiService,
    public router: Router
  ) { }

  ngOnInit(): void {
    this.checkLogin();
    this.menu();
  }
  menu(){
    this.items=[
      {icon:'dashboard', text:'Dasboard', link:'/admin/dashboard'},
      {icon:'camera_enhance', text:'Product', link:'/admin/produk'} 
    ]
  }

  checkLogin()
  {
    this.api.get('auth/status').subscribe(res=>{
      //is logged in
      return;
    },err=>{
      //not logged in
      this.router.navigate(['/login']);
    })

  }
  logout()
  { 
    let conf=confirm('keluar aplikasi?');
    if(conf)
    { 
     localStorage.removeItem('appToken');
     window.location.reload(); 
    }
  }
}
